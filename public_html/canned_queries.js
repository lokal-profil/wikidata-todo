var wd = new WikiData() ;
var cqapi = 'canned_queries.php' ;
var cur = {
	start : 0 ,
	size : 100 ,
	d0 : {} ,
	params : {}
} ;

var wdq_url = 'http://wdq.wmflabs.org/api?callback=?' ;

function getItemLink ( q ) {
	q = (q+'').replace ( /\D/g , '' ) ;
	return "<a href='//www.wikidata.org/wiki/Q" + q + "' target='_blank'>Q" + q + "</a>" ;
}

function born_before_parents () {
	$('#loading').show() ;
	$('#results').css ( { 'background-color' : '#DDDDDD' } ) ;
	$.getJSON ( wdq_url , {
		q : 'claim[40,22,25] AND claim[569]' ,
		props : '40,22,25,569'
	} , function ( d ) {
//		console.log ( d ) ;
		
		var parent_of = {} ;
		var born = {} ;
		$.each ( d.props[22] , function ( k , v ) { if(v[1]!='item')return; if(undefined===parent_of[v[0]])parent_of[v[0]]=[]; parent_of[v[0]].push(v[2]) } ) ;
		$.each ( d.props[25] , function ( k , v ) { if(v[1]!='item')return; if(undefined===parent_of[v[0]])parent_of[v[0]]=[]; parent_of[v[0]].push(v[2]) } ) ;
		$.each ( d.props[40] , function ( k , v ) { if(v[1]!='item')return; if(undefined===parent_of[v[2]])parent_of[v[2]]=[]; parent_of[v[2]].push(v[0]) } ) ;
		$.each ( d.props[569] , function ( k , v ) { if(v[1]!='time')return; born[v[0]]=v[2] } ) ;
		
		var show = {} ;
		var items = [] ;
		$.each ( born , function ( q , v ) {
			var time1 = v.replace(/-/g,'0') ;
			if ( undefined === parent_of[q] ) return ; // No parent
			$.each ( parent_of[q] , function ( dummy , q2 ) {
				if ( undefined === born[q2] ) return ;
				var time2 = born[q2].replace(/-/g,'0') ;
				if ( time1 > time2 ) return ; // OK
				var key = q+'/'+q2 ;
				if ( undefined !== show[key] ) return ; // Had that
				show[key] = [ q , time1 , q2 , time2 ] ;
				items.push ( q ) ;
				items.push ( q2 ) ;
			} ) ;
		} ) ;

		wd.loadItems ( items , { finished : function () {
			var h = "<ol>" ;
			$.each ( show , function ( k , v ) {
				var i1 = wd.getItem ( 'Q'+v[0] ) ;
				var i2 = wd.getItem ( 'Q'+v[2] ) ;
				var t1 = i1.getClaimObjectsForProperty(569)[0].time ;
				var t2 = i2.getClaimObjectsForProperty(569)[0].time ;
				if ( t1 > t2 ) return ; // Everything OK
				if ( t1.substr(0,1) == '-' && t2.substr(0,1) == '-' ) return ; // BCE, OK
				t1 = t1.replace(/T.+$/,'').replace(/^(.)0+/,t1.substr(0,1)) ;
				t2 = t2.replace(/T.+$/,'').replace(/^(.)0+/,t2.substr(0,1)) ;
				h += "<li>Child "+i1.getLink()+" ("+t1+") born before parent "+i2.getLink()+" ("+t2+")</li>" ;
			} ) ;
			h += "</ol>" ;

			$('#loading').hide() ;
			$('#results').css ( { 'background-color' : 'white' } ) ;
			$('#results').html ( h ) ;
		} } ) ;
		
	} ) ;
}

function people_without_gender ( occupation ) {
//	var query = 'claim[106,107:215627] AND noclaim[21]' ; // Occupation or GND:person
	cur.query = 'claim[106' + (occupation===undefined?'':':'+occupation) + '] AND noclaim[21]' ;
	cur.params = { skip_if_proplink:[21] } ;
	initQuery () ;
}

function parents_without_gender () {
	cur.query = 'claim[40] AND noclaim[21]' ;
	cur.params = { skip_if_proplink:[21] } ;
	initQuery () ;
}

function initQuery () {
	cur.start = 0 ;
	$('#results').html('') ;
	$('#loading').show() ;
	$.getJSON ( wdq_url , {
		q : cur.query
	} , function ( d0 ) {
		cur.d0 = d0 ;
		showCur() ;
	} ) ;


}

function showPrev() {
	cur.start -= cur.size ;
	showCur () ;
}

function showNext() {
	cur.start += cur.size ;
	showCur () ;
}

function showCur () {
	var items = [] ;
	if ( cur.start < 0 ) cur.start = 0 ; // Paranoia
	for ( var p = cur.start ; p <= cur.start + cur.size * 2 ; p++ ) {
		if ( undefined !== cur.d0.items[p] ) items.push ( cur.d0.items[p] ) ;
	}
	
	$('#loading').show() ;
	$('#results').css ( { 'background-color' : '#DDDDDD' } ) ;
	$.post ( cqapi , {
		items : items.join(',') ,
		params : JSON.stringify ( cur.params )
	} , function ( d ) {
		$.each ( d.items , function ( k , v ) {
			d.items[k] = 'Q' + v ;
		} ) ;
		wd.loadItems ( d.items , { finished : function () {
			$('#loading').hide() ;
			var h = '' ;
			h += "<ol start='" + (cur.start+1) + "'>" ;
			var cnt = 0 ;
			$.each ( d.items , function ( k , q ) {
				if ( cnt >= cur.size ) return false ;
				var i = wd.getItem ( q ) ;
				h += "<li>" ;
				h += "<a target='_blank' href='//www.wikidata.org/wiki/" + q + "'>" + i.getLabel() + "</a>" ;
				h += " <small>" + i.getDesc() + "</small>" ;
				h += "</li>" ;
				cnt++ ;
			} ) ;
			h += "</ol>" ;
			$('#results').css ( { 'background-color' : 'white' } ) ;
			$('#results').html ( h ) ;
			
			h = '' ;
			h += "<div style='float:right'><a href='#' onclick='showCur();return false'>↻</a></div>" ;
			if ( cur.start > 0 ) {
				h += "<a href='#' onclick='showPrev();return false'>" + ( cur.start - cur.size + 1 ) + "&mdash;" + ( cur.start )  + "</a> " ;
			}
			h += "<b>" + (cur.start+1) + "&mdash;" + (cur.start+cur.size) + "</b>" ;
			h += " <a href='#' onclick='showNext();return false'>" + ( cur.start + cur.size + 1 ) + "&mdash;" + ( cur.start + cur.size*2 ) + "</a>" ;
			h += " of " + cur.d0.items.length + " total" ;
			$('#nav_top').show().html ( h ) ;
			$('#nav_bottom').show().html ( h ) ;
			
		} } ) ;
	} , 'json' ) ;
}

$(document).ready ( function () {
	if ( parent.location.protocol == 'https:' ) window.location = 'http://tools.wmflabs.org/wikidata-todo/canned_queries.html' ; // Force http
} ) ;
