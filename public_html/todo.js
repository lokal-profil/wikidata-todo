var wd_todo = {

	api : '/w/api.php' ,

	init : function () {
		var self = this ;
 		$('body').append ( "<div id='todo-dialog' style='display:none'></div>" ) ;
		var portletLink = mw.util.addPortletLink( 'p-tb', 'javascript:;', 'ToDo','t-wd_todo');
		$(portletLink).click ( function () {
			self.run(true) ;
			return false ;
		} ) ;
	} ,
	
	run : function () {
		var self = this ;
		var h = "<div style='border-bottom:2px solid black;padding:2px;margin-bottom:5px;'>" ;
		h += "<a href='#' onclick='wd_todo.load(\"nogender\");return false'>Genderless</a>" ;
		h += "</div>" ;
		h += "<div id='todo_main' style='width:800px;height:620px;overflow:auto'></div>" ;
		
		$('#todo-dialog').html(h).show().dialog ( {
			modal : true ,
			width:'auto' ,
			title : "Wikidata ToDo"
		} ) ;
		self.load ( 'nogender') ; // Default
	} ,
	
	fixattr : function ( s ) {
		return s.replace ( /\'/g , '' ) ;
	} ,
	
	load : function ( key ) {
		var self = this ;
		$('#todo_main').html('<i>Loading...</i>') ;
		var url = '//tools.wmflabs.org/wikidata-todo/?bot=1&action=' + key + "&max=60&callback=?" ;
		$.getJSON ( url , function ( d ) {
			var h = '' ;
			if ( key == 'nogender' ) {
				h += "<div>People without assigned gender property left : " + d.total + "</div>" ;
			}
			h += "<div style='-moz-column-count:2;-webkit-column-count:2;column-count:2;'>" ;
			$.each ( d.pages , function ( k , v ) {
				h += "<div id='todo_" + k + "'>";
				if ( key == 'nogender' ) {
					var firstname = v.split(/\s/g).shift().toLowerCase() ;
					h += "<input type='checkbox' class='todo_cb' key='" + self.fixattr(firstname) + "' q='" + k + "'/>" ;
					h += "<a href='#' onclick='wd_todo.setProp(\""+k+"\",21,6581097);return false'>♂</a>" ;
					h += " | <a href='#' onclick='wd_todo.setProp(\""+k+"\",21,6581072);return false'>♀</a> " ;
				}
				h += "<a class='todo_link' href='/wiki/" + k + "' target='_blank'>" + v + "</a>" ;
				h += "</div>" ;
			} ) ;
			h += "</div>" ;
			$('#todo_main').html ( h ) ;
			$('input.todo_cb').change ( function (e) {
//				e.preventDefault() ;
				var key = $(this).attr('key') ;
//				console.log ( key ) ;
				var cb = $('input.todo_cb[key="'+self.fixattr(key)+'"]') ;
//				console.log ( cb.length ) ;
				if ( cb.length == 1 ) return true ;
				cb.prop("checked", !cb.prop("checked"));
				return false ;
			} ) ;
		} ) ;
	} ,
	
	setProp : function ( oq , p , q2 ) {
		var self = this ;
		oq += '' ;
		p += '' ;
		q2 += '' ;
		
		$('#todo_'+oq+' input.todo_cb').prop('checked','checked') ;
		$('input.todo_cb').each ( function () {
			var x = $(this) ;
			if ( !$(x).is(':checked') ) return ;
			var q = $(x).attr('q') ;
			var val = '{"entity-type":"item","numeric-id":' + q2.replace(/\D/g,'') + '}' ;
			self.createClaim ( q , p , val , function () {
				var o = $('#todo_'+q+' a.todo_link') ;
				o.html ( "<s>" + o.html() + "</s>" ) ;
				$('#todo_'+q+' input.todo_cb').replaceWith('&nbsp;X&nbsp;');
//				$('#todo_'+q+' a').replaceWith(function() { return "<span>" + $(this).html() + "</span>" ; });
			} ) ;
		} ) ;
	} ,



	createClaim : function ( entity , property , value , callback ) {
		var self = this ;
		entity = 'q' + entity.replace ( /\D/g , '' ) ;
		property = property.replace ( /\D/g , '' ) ;
		$.post ( self.api , {
			action : 'query' ,
			prop : 'info' ,
			intoken : 'edit' ,
			titles : entity ,
			format : 'json'
		} , function ( data ) {
			var token , lastrevid ;
			$.each ( (data.query.pages||[]) , function ( k , v ) {
				token = v.edittoken ;
				lastrevid = v.lastrevid ;
			} ) ;
 
			if ( undefined === token ) {
				console.log ( "Cannot get edit token for " + entity ) ;
				return ;
			}
 
			property = property.replace(/\D/g,'') ;
			entity = entity.replace(/\D/g,'') ;
			var vo = JSON.parse ( value ) ;
			var is_item_target = vo['numeric-id'] !== undefined ;
			var value_id = is_item_target ? vo['numeric-id']+'' : vo ;
 
			var other_entity = ( entity.replace(/\D/g,'') != entity.replace(/\D/g,'') ) ;
 
			$.post ( self.api , {
				action : 'wbcreateclaim' ,
				entity : 'q'+entity ,
				snaktype : 'value' ,
				property : 'p'+property ,
				value : value ,
				token : token ,
				baserevid : lastrevid ,
				format : 'json'
			} , function ( data ) {
				callback() ;
			} , 'json' ) ;
 		} , 'json' ) ;
 
	} ,
 
	fin : ''
} ;

addOnloadHook ( function() {
	if ( mw.config.get('wgNamespaceNumber') != 0 ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;
 
	wd_todo.init () ;
});