<?PHP

require_once ( 'php/common.php' ) ;

print get_common_header ( '' , 'Wikidatas item with label pattern but without "instance of"' ) ;

$lang = get_request ( 'lang' , '' ) ;
$pattern = get_request ( 'pattern' , '' ) ;

print "<div class='lead'>Enter a language code and a MySQL LIKE pattern (e.g. \"Burg %\"), and get a list of items with matching labels but no \"instance of\".</div>" ;
print "<form method='get' action='?'><input type='text' name='lang' value='$lang' placeholder='lang'/><input type='text' name='pattern' value='$pattern' placeholder='Pattern' /><input type='submit' name='run' class='btn btn-primary' /></form>" ;

if ( isset ( $_REQUEST['run'] ) ) {

print "<hr/>" ;

	$db = openDB ( 'wikidata' , '' ) ;
	$lang = $db->real_escape_string ( $lang ) ;
	$pattern = $db->real_escape_string ( $pattern ) ;

	$sql = 'select distinct page_title from page,wb_terms,wb_entity_per_page where page_namespace=0 and not exists ( select * from pagelinks where page_id=pl_from and pl_namespace=120 and (pl_title="P31" OR pl_title="P279") limit 1)' ;
	$sql .= " and term_language='$lang' and term_type='label' and term_entity_type='item' and term_entity_id=epp_entity_id and epp_entity_type='item' and term_text like '$pattern' and epp_page_id=page_id" ;

	print "<h2>Results</h2><form target='_blank' method='post' action='./autolist2.php'><textarea name='manual_list' rows='10'>" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$cnt = 0 ;
	while($r = $result->fetch_object()){
		print $r->page_title . "\n" ;
		$cnt++ ;
	}
	print "</textarea><br/><input type='submit' class='btn btn-primary' name='run' value='Autolist2'></form>" ;
	print "<div>$cnt results total.</div>" ;
}

print get_common_footer() ;

/*
$r = rand() /  getrandmax() ;
$sql = 'select * from page where page_namespace=0 and not exists ( select * from pagelinks where page_id=pl_from and pl_namespace=120 and pl_title="P31" limit 1) and page_random>=' . $r ;
if ( $lang != '' ) {
	$lang = $db->real_escape_string ( $lang ) ;
	$sql .= " and exists (select * from wb_entity_per_page,wb_items_per_site where ips_site_id='$lang' and ips_item_id=epp_entity_id and epp_entity_type='item' and epp_page_id=page_id limit 1)" ;
}
$sql .= ' order by page_random limit 1' ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($r = $result->fetch_object()){
	print "<html><head><meta http-equiv='refresh' content='0; url=//www.wikidata.org/lang/" . $r->page_title . "' /></head></html>" ;
}
*/

?>