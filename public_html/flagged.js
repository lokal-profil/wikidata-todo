$( function() {
	var flaggedItemsPage = "Wikidata:The_Game/Flagged_items";

	if (
		mw.config.get( 'wgAction' ) !== 'view' ||
		mw.config.get( 'wgPageName' ) !== flaggedItemsPage
	) return;

	$( '#mw-content-text h3' ).each( function () {
		var $a = $( this ).find( 'span.mw-editsection a' ).eq( 0 );
		$a.after(
			$( '<span>' ).append(
				document.createTextNode( ' / ' ),
				$( '<a>' )
					.attr( {
						'href': '#',
						'class': 'flagged_delete_section'
					} )
					.data( 'section', new mw.Uri( $a.attr( 'href' ) ).query.section )
					.text( 'delete' )
			)
		);
	} );

	$( 'a.flagged_delete_section' ).click( function () {
		var section = $( this ).data( 'section' ),
			h3 = $( this ).parents( 'h3' ).eq( 0 );
		h3.css( { 'background-color': '#DDD' } );
		new mw.Api().postWithToken( 'edit', {
			action: 'edit',
			title: flaggedItemsPage,
			section: section,
			text: '',
		} ).done( function () {
			// Remove HTML elements
			h3.next().remove();
			h3.remove();
			$( 'a.flagged_delete_section' ).each( function () {
				var a = $( this ),
					s2 = a.data( 'section' );
				if ( s2 < section ) return;
				a.data( { section: s2 - 1 } );
			} );
		} );
		return false;
	} );
} );