var wd_autolist = {
	api : 'http://208.80.153.172/api' ,
	query : '' ,
	offset : 0 ,
	listlen : 50 ,
	show_props : '' ,
	loaded : false ,
	
	init : function () {
		var self = this ;
		self.wd = new WikiData () ;
	}  ,
	
	setLanguage : function ( l ) {
		var self = this ;
		if ( self.wd.language == l ) return ; // Nothing to do
		self.wd.language = l ;
		self.wd.main_languages.unshift ( l ) ;
		if ( wd_auto_desc.lang != l ) {
			wd_auto_desc.lang = l ;
			wd_auto_desc.cache = {} ;
		}
		self.show() ;
	} ,
	
	runCatQuery : function ( cat_name , cat_lang , cat_project ) {
		var self = this ;
		$('#loading').html("Running query...").show() ;
		$.getJSON ( '//tools.wmflabs.org/catscan2/quick_intersection.php?callback=?' , {
			lang : cat_lang ,
			project : cat_project ,
			cats : cat_name ,
			ns : 0 ,
			depth : 12 ,
			max : 30000 ,
			start : 0 ,
			wikidata : 1 ,
			format : 'json'
		} , function ( d ) {
			self.d = {
				status : { error : 'OK' , items : 0 } ,
				items : []
			} ;
			$.each ( d.pages , function ( k , v ) {
				self.d.status.items++ ;
				if ( v.q !== undefined ) self.d.items.push ( v.q.replace(/\D/g,'') ) ;
			} ) ;
			$('#il_addon').html('('+self.d.status.items+' pages with '+self.d.items.length+' items)') ;
			$('#loading').hide() ;
			self.loaded = true ;
			self.show() ;
		} ) ;
	} ,

	runQuery : function ( query ) {
		var self = this ;
		self.query = $.trim ( query ) ;
//		self.offset = 0 ;
		$('#nav_top').hide() ;
		$('#results').hide() ;
		$('#nav_bottom').hide() ;
		$('#il_addon').html('') ;
		if ( self.query == '' ) return ;
		$('#loading').html("Running query...").show() ;
		$.getJSON ( self.api+'?callback=?' , {
			q : self.query
		} , function ( d ) {
			self.d = d ;
			if ( d.status.error != 'OK' ) {
				return ;
			}
			self.loaded = true ;
			$('#loading').hide() ;
			$('#il_addon').html('('+self.d.status.items+' items)') ;
			self.show() ;
		} ) ;
	} ,
	
	updatePermalink : function () {
		var self = this ;
		var href = 'http://tools.wmflabs.org/wikidata-todo/autolist.html?' ;
		if ( self.wd.language != 'en' ) href += 'lang=' + self.wd.language + '&' ;
		if ( self.offset != 0 ) href += 'start=' + self.offset + '&' ;
		if ( self.listlen != 50 ) href += 'listlen=' + self.listlen + '&' ;
		if ( self.show_props != '' ) href += 'props=' + self.show_props + '&' ;
		href += 'q=' + escape ( self.query ) ;
		$('#permalink').attr({'href':href}) ;
		$('#twitter').attr({'href':'http://twitter.com/intent/tweet?url='+escape(href)}) ;

		$('#email').attr({'href':'mailto:?to=&subject=Wikipedia/Wikidata item list&body='+escape(href)}) ;
		
		$('#links').show() ;
	} ,
	
	getNavbar : function () {
		var self = this ;
		var h = [] ;
		var max = self.d.items.length ; //self.d.status.items ;
		for ( var i = 0 ; i < max ; i += self.listlen ) {
			var from = i+1 ;
			var to = i+self.listlen ;
			if ( to > max ) to = max ;
			if ( i == self.offset ) {
				h.push ( "<b>" + from + "-" + to + "</b>" ) ;
			} else {
				h.push ( "<a href='#' onclick='wd_autolist.seek("+i+")'>" + from + "-" + to + "</a>" ) ;
			}
		}
		h = "<div style='max-height:40px;overflow:auto'>" + h.join ( ' | ' ) + "</div>" ;
		return h ;
	} ,
	
	seek : function ( pos ) {
		var self = this ;
		self.offset = pos ;
		self.show() ;
		return false
	} ,
	
	show : function () {
		var self = this ;
		if ( !self.loaded ) return ;
		var items = [] ;
		var h = "<table class='table table-condensed table-striped'>" ;
		h += "<thead><tr><th>#</th><th class='oauth'></th><th>Item</th><th>Description</th><th>Wikipedia(s)</th>" ;
		if ( self.show_props != '' ) h += "<th>Props</th>" ;
		h += "</tr></thead><tbody>" ;
		
		for ( var i = 0 ; i < self.listlen && i+self.offset < self.d.items.length ; i++ ) {
			var q = self.d.items[i+self.offset] ;
			items.push ( 'Q'+q ) ;
			h += "<tr id='Q" + q + "'>" ;
			h += "<th>"+(i+self.offset+1)+"</th><td><a target='_blank' href='//www.wikidata.org/wiki/Q" + q + "'>Q" + q + "</a></td><td colspan=3 />" ;
			h += "</tr>" ;
		}
		h += '</tbody></table>' ;
		
		var navbar = self.getNavbar() ;
		
		$('#nav_top').html(navbar).show() ;
		$('#results').html(h).show() ;
		$('#nav_bottom').html(navbar).show() ;
		
		var l = self.wd.language ;
		$('#loading').html("Getting labels...").show() ;
		self.wd.loadItems ( items , { finished : function () {
			$.each ( items , function ( dummy , q ) {
				var i = self.wd.getItem ( q ) ;
				var desc = i.getDesc() ;
				var h = '' ;
				h += "<th>" + (dummy+self.offset+1) + "</th>" ;
				h += "<td class='oauth'><input type='checkbox' q='" + q + "' /></td>" ;
				h += "<td>" + i.getLink ( { target:'_blank' } ) + "</td>" ;
				h += "<td><small id='desc_"+q+"'>" + (desc==''?'<i>generating...</i>':desc) + "</small></td>" ;
				var links = i.getWikiLinks() ;
				
				h += "<td>" ;
				h += "<div>" ;
				if ( undefined === links[l+'wiki'] ) {
					h += "<a target='_blank' style='color:red !important' href='//" + l + ".wikipedia.org/w/index.php?title=" + escape(i.getLabel()) + "&action=edit'>" + l + ".wikipedia</a>" ;
				} else {
					h += "<a target='_blank' href='//" + l + ".wikipedia.org/wiki/" + escape(links[l+'wiki'].title) + "'>" + l + ".wikipedia</a>" ;// (" + links[l+'wiki'].title + ")" ;
				}
				h += "</div>" ;
				
				var other = [] ;
				$.each ( links , function ( lang , v ) {
					if ( lang == l+'wiki' ) return ;
					var m = lang.match ( /(.+)wiki$/ ) ;
					if ( m == null ) {
						other.push ( lang ) ;
						return ;
					}
					other.push ( "<a target='_blank' href='//"+m[1]+".wikipedia.org/wiki/"+escape(v.title)+"'>"+m[1]+"</a>" ) ;
				} ) ;
				h += "<div style='max-width:400px;font-size:9pt'>" + other.join(', ') + "</div>" ;
				h += "</td>" ;
				
				if ( self.show_props != '' ) {
					h += "<td nowrap>" ;
					$.each ( self.show_props.split(',') , function ( k , prop ) {
						if ( !i.hasClaims(prop) ) return ;
						$.each ( i.getClaimsForProperty(prop) , function ( k2 , v2 ) {
							h += "<div title='P"+prop+"'>" ;
							if ( v2.mainsnak !== undefined && v2.mainsnak.datavalue !== undefined ) {
								var v = v2.mainsnak.datavalue ;
								if (v.type == 'time' ) {
									h += v.value.time.substr(8,10) ;
									if ( v.value.time.substr(0,1)=='-' ) h += "BCE" ;
								} else if (v.type == 'string' ) {
									h += v.value ;
								} else if ( v.type == 'globecoordinate' ) {
									h += Math.floor(v.value.latitude*1000)/1000 + '/' + Math.floor(v.value.longitude*1000)/1000 ;
								} else if (v.type == 'wikibase-entityid' ) {
									h += "<a target='_blank' href='//www.wikidata.org/wiki/Q" + v.value['numeric-id'] + "'>Q" + v.value['numeric-id'] + "</a>" ;
								} else console.log ( v2 ) ;
							} else console.log ( v2 ) ;
							h += "</div>" ;
						} ) ;
					} ) ;
					h += "</td>" ;
				}
				
				$('#'+q).html ( h ) ;


				if ( desc == '' ) {
					wd_auto_desc.loadItem ( q , {
						target:$('#desc_'+q) ,
			//			callback : function ( q , html , opt ) { console.log ( q + ' : ' + html ) } ,
			//			links : 'wikidata' , // wikipedia
						linktarget : '_blank'
					} ) ;
				}


			} ) ;
			$('#loading').hide() ;
			if ( $('#cb_has_oauth').is(':checked') ) $('.oauth').show() ;
		} } ) ;
		self.updatePermalink() ;
	} ,
	
	claim_it : function ( e ) {
		var self = wd_autolist ;
		var ids = [] ;
		$('td.oauth input').each ( function ( k , v ) {
			if ( !$(v).is(':checked') ) return ;
			ids.push ( $(v).attr('q') ) ;
		} ) ;

		function doit () {
			var prop = 'P' + $('#claim_dialog_prop').val().replace(/\D/g,'') ;
			var target = 'Q' + $('#claim_dialog_target').val().replace(/\D/g,'') ;
			
			if ( prop == '' || target == '' ) {
				$('#claim_dialog button.close').click() ;
				return false ;
			}

			$.cookie('autolist_last_prop', prop , { expires: 90, path: '/' });
			$.cookie('autolist_last_target', target , { expires: 90, path: '/' });
			
			var url = "http://tools.wmflabs.org/widar/index.php?action=set_claims&ids="+ids.join(",")+"&prop="+prop+"&target="+target ;
			window.open(url);//,'WiDaR');
			$.each ( ids , function ( k , v ) {
				v = v.replace(/\D/g,'') * 1 ;
				self.d.items = $.grep ( self.d.items , function ( value ) {
					return value != v ;
				} ) ;
			} ) ;
			self.show() ;
			$('#claim_dialog button.close').click() ;
			return false ;
		}
		
		$('#claim_dialog_button_ok').unbind('click');
		$('#claim_dialog_button_ok').click ( doit ) ;
		$('#claim_dialog').modal( {
			shown : function () {
				if ( $.cookie('autolist_last_prop') !== undefined ) $('#claim_dialog_prop').val ( $.cookie('autolist_last_prop') ) ;
				if ( $.cookie('autolist_last_target') !== undefined ) $('#claim_dialog_target').val ( $.cookie('autolist_last_target') ) ;
				$('#claim_dialog_prop').focus() ;
			}
		} ) ;
		
		return false ;
	} ,
	
	
	the_end : ''
}


$(document).ready ( function () {
	if ( parent.location.protocol == 'https:' ) {
		window.location = 'http://tools.wmflabs.org/wikidata-todo/autolist.html' ; // Force http
		return ;
	}
	
	
	function updateLanguage () {
		wd_autolist.setLanguage ( $('#language').val() ) ;
	}
	
	function updateProps () {
		wd_autolist.show_props = $('#props').val() ;
		wd_autolist.show() ;
	}
	
	function param( name ) {
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( window.location.href );
	  if( results == null )
		return "";
	  else
		return unescape(results[1]);
	}
	
	$('#claim_it').tooltip();
	$('#widar').tooltip();
	
	// OAuth
	$('#cb_has_oauth').change ( function () {
		$('.oauth').toggle() ;
		$.cookie('widar_oauth', $('#cb_has_oauth').is(':checked')?1:0 , { expires: 90, path: '/' });
	} ) ;
	$('#claim_it').click ( wd_autolist.claim_it ) ;
	if ( $.cookie('widar_oauth') == 1 ) {
		$('#cb_has_oauth').prop('checked',true) ;
		$('#cb_has_oauth').change();
	}
	
	var cat_name = param('cat_name') ;
	var cat_lang = param('cat_lang') ;
	var cat_project = param('cat_project') ;
	$('#cat_name').val ( cat_name ) ;
	$('#cat_lang').val ( cat_lang==''?'en':cat_lang ) ;
	$('#cat_project').val ( cat_project==''?'wikipedia':cat_project ) ;
	$('#cat_run').click ( function () {
		wd_autolist.offset = 0 ;
		wd_autolist.runCatQuery ( $('#cat_name').val() , $('#cat_lang').val() , $('#cat_project').val() ) ;
	} ) ;

	var query = param('q') ;
	$('#query').val ( query ) ;
	var l = param('lang') ;
	if ( l != '' ) $('#language').val ( l ) ;
	wd_autolist.show_props = param('props') ;
	$('#props').val ( wd_autolist.show_props ) ;
	$('#main_form').submit ( function ( e ) { return false } ) ;
	wd_autolist.init() ;
	$('#language_update').click ( updateLanguage ) ;
	$('#props_update').click ( updateProps ) ;
	$('#run').click ( function () {
		wd_autolist.offset = 0 ;
		wd_autolist.runQuery ( $('#query').val() ) ;
	} ) ;
	updateLanguage() ;
	if ( query != '' ) {
		wd_autolist.offset = param('start')*1 ;
		if ( param('listlen') != '' ) wd_autolist.listlen = param('listlen')*1 ;
		wd_autolist.runQuery ( query ) ;
	} else if ( cat_name != '' ) {
		wd_autolist.offset = param('start')*1 ;
		if ( param('listlen') != '' ) wd_autolist.listlen = param('listlen')*1 ;
		wd_autolist.runCatQuery ( $('#cat_name').val() , $('#cat_lang').val() , $('#cat_project').val() ) ;
	}
	$('#query').focus();
} ) ;
