#!/bin/bash
echo "START"
cd /data/project/wikidata-todo/scripts/wikispecies
\rm qs.tab qs2.tab qs3.tab botanists.add disam.add qs4.tab people.create qs5.tab

./__match_taxon.php > qs4.tab
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/qs4.tab

./__match_or_create_species.php > qs.tab
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/qs.tab

./__find_genus.php > qs2.tab
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/qs2.tab

./disam.php
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/disam.add

./botanists.php
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/botanists.add

./find_all_taxa.php
./taxon_creator.php
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/taxa.create

# Give SPARQL a chance to catch up
sleep 3m

./parent_taxon.php > qs3.tab
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/qs3.tab


./create_new_people.php
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/people.create

./misc.php > qs5.tab
/data/project/wikidata-todo/scripts/quick_statements.php /data/project/wikidata-todo/scripts/wikispecies/qs5.tab


cd /data/project/wikidata-todo/scripts/duplicity_bot
./bot.php specieswiki

echo "DONE"